function btnSec() {
    var oneDayWage = document.getElementById('oneDayWage').value*1;
    var workingDay = document.getElementById('workingDay').value*1;
    var resultSec1 = oneDayWage * workingDay;
    document.getElementById('resultSec1').innerText = resultSec1.toLocaleString();
}

function btnSec2() {
    var no1st = document.getElementById('no1st').value*1;
    var no2nd = document.getElementById('no2nd').value*1;
    var no3rd = document.getElementById('no3rd').value*1;
    var no4th = document.getElementById('no4th').value*1;
    var no5th = document.getElementById('no5th').value*1;

    var resultSec2 = (no1st + no2nd + no3rd + no4th + no5th)/5;
    document.getElementById('resultSec2').innerText = resultSec2.toLocaleString();
}

function btnSec3() {
    var USDAmount = document.getElementById('USDAmount').value*1;
    var USDExchangeRate = 23500;
    var VNDAmount = USDAmount * USDExchangeRate;
    document.getElementById('resultSec3').innerHTML = `VND ${VNDAmount.toLocaleString()}`;
}

function btnSec4() {
    var length = document.getElementById('length').value*1;
    var width = document.getElementById('width').value*1;
    
    var perimeter = (length+width)*2;
    var area = length*width;
    var resultSec4 = document.getElementById('resultSec4').innerText = `Chu vi: ${perimeter.toLocaleString()}; Diện tích: ${area.toLocaleString()}`;
}

function btnSec5() {
    var no = document.getElementById('no').value*1;
    var dozens = Math.floor(no/10);
    var units = no%10;
    var resultSec5 = dozens + units;
    document.getElementById('resultSec5').innerText = resultSec5;
}